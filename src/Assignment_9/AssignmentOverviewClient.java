package Assignment_9;

import java.util.Scanner;

public class AssignmentOverviewClient {
    /** Main method for assignment overview class
     * @param args -> command line arguments
     */
    public static void main(String[] args) {
        int choice;
        Scanner scanner = new Scanner(System.in);
        AssignmentOverview overview = new AssignmentOverview();
        System.out.println("Welcome to the assignment overview!");
        do {
            System.out.println("\nTo display the assignment overview, enter 1 \n" +
                    "To register a student, enter 2\n" +
                    "To increase a student's assignments, choose 3\n" +
                    "To exit the program, choose 4\n");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.println(overview);
                    break;
                case 2:
                    System.out.println("Please enter the name of the student you want to register:");
                    String name1 = scanner.nextLine();
                    if (!name1.isEmpty()) {
                        overview.registerStudent(name1);
                    } else {
                        System.out.println("Student name cannot be empty, please try again.");
                    }
                    System.out.println("Student registered!");
                    break;
                case 3:
                    System.out.println("Please enter the name of the student you want to increase the number of assignments for:");
                    String name2 = scanner.nextLine();
                    if (name2.isEmpty() || !overview.studentExists(name2)) {
                        System.out.println("Invalid student name, please try again.");
                        break;
                    }
                    System.out.println("Please enter the number of assignments you want to increase:");
                    int increase = scanner.nextInt();
                    scanner.nextLine();
                    overview.increaseAssignmentsPerStudent(name2, increase);
                    System.out.println("Assignments increased!");
                    break;
                default:
                    if (choice != 4) {
                        System.out.println("Invalid input, please try again");
                        scanner.nextLine();
                    }
                    break;
            }

        } while (choice != 4);
        scanner.close();
        System.exit(0);
    }
}
