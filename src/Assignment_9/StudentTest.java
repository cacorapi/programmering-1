package Assignment_9;

import org.junit.Test;
import static org.junit.Assert.*;

public class StudentTest {
    @Test
    public void testStudent() {
        Student student = new Student("Charlotte", 0);
        assertEquals("Charlotte", student.getName());
        assertEquals(0, student.getNumAssignments());
    }
    @Test
    public void testIncreaseNumAssignments() {
        Student student = new Student("Charlotte", 0);
        student.increaseNumAssignments(2);
        assertEquals(2, student.getNumAssignments());
    }
}
