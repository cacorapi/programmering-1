package Assignment_9;

import java.util.ArrayList;

public class AssignmentOverview {
    private final ArrayList<Student> students;
    private int numStudents;

    /** Constructor for AssignmentOverview
     */
    public AssignmentOverview() {
        this.students = new ArrayList<>();
        this.numStudents = 0;
    }

    /** Getters for AssignmentOverview
        * @return -> list of students and number of students
        * @throws IllegalArgumentException -> if student not found
    */
    public int getNumStudents() {
        return this.numStudents;
    }
    public int getAssignmentsPerStudent(String name) throws IllegalArgumentException{
        for (Student student: students) {
            if (student.getName().equals(name)) {
                return student.getNumAssignments();
            }
        } throw new IllegalArgumentException("Student not found");
    }
    public ArrayList<Student> getStudents() {
        return this.students;
    }

    /** Method to register a student
     * @param name -> name of student
     * @throws IllegalArgumentException -> if student name is not found
    */
    public void registerStudent(String name) throws IllegalArgumentException{
        for (Student student : students) {
            if (student.getName().equals(name)) {
                throw new IllegalArgumentException("Student already exists");
            }
        }
        students.add(new Student(name, 0));
        this.numStudents++;
    }

    /** Method to increase the number of assignments per student
     * @param name -> name of student
     * @param increase -> number to add to student's assignments
     * @throws IllegalArgumentException if student not found
     */
    public void increaseAssignmentsPerStudent(String name, int increase) throws IllegalArgumentException {
        for (Student student : students) {
            if (student.getName().equals(name)) {
                student.increaseNumAssignments(increase);
                return;
            }
        }
        throw new IllegalArgumentException("Student not found");
    }
    /** Method to check if a student exists in the registry
     * @param name -> name of student
     * @return -> true if student exists, false if not
     */
    public boolean studentExists(String name) {
        for (Student student :getStudents()) {
            if (student.getName().equals(name)) {
                return true;
            }
        } return false;
    }

    /** toString Method
     * @return -> string representation of AssignmentOverview
     */
    public String toString() {
        StringBuilder builder = new StringBuilder("Total students and their information:\n");
        for (Student student : students) {
            builder.append(student.toString()).append("\n");
        }
        return builder.toString();
    }
}
