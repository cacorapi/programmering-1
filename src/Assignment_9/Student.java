package Assignment_9;

public class Student {
    private final String name;
    private int numAssignments;

    /** Constructor for Student
     * @param name -> name of student
     * @param numAssignments -> number og assignments per student
     */
    public Student(String name, int numAssignments) {
        this.name = name;
        this.numAssignments = numAssignments;
    }

    /** Getters for Student
     * @return -> name and number of assignments
     */
    public String getName() {
        return this.name;
    }
    public int getNumAssignments() {
        return this.numAssignments;
    }

    /** Method to increase number of assignments per student
     * @param increase -> number of assignments to increase
     */
    public void increaseNumAssignments(int increase) {
        this.numAssignments += increase;
    }

    /** toString Method
     * @return -> string representation of student
     */
    public String toString() {
            return getName() + " has completed " + getNumAssignments() + " assignments.";
    }
}
