package Assignment_2;

public class Oppgave_1 {
    double year;

    public Oppgave_1(double year) {
        this.year = year;
    }

    public boolean isLeapYear() {
        if (this.year % 4 == 0) {
            if (this.year < 1000 && this.year % 400 != 0) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Oppgave_1 test_1 = new Oppgave_1(480);
        System.out.println(test_1.isLeapYear());
        if (test_1.isLeapYear() == false) {
            System.out.println("Test 1 passed");
        } else {
            System.out.println("Test 1 failed");
        }

        Oppgave_1 test_2 = new Oppgave_1(2020);
        System.out.println(test_2.isLeapYear());
        if (test_2.isLeapYear() == true) {
            System.out.println("Test 2 passed");
        } else {
            System.out.println("Test 2 failed");
        }

        Oppgave_1 test_3 = new Oppgave_1(800);
        System.out.println(test_3.isLeapYear());
        if (test_3.isLeapYear() == true) {
            System.out.println("Test 2 passed");
        } else {
            System.out.println("Test 2 failed");
        }



    }

}

