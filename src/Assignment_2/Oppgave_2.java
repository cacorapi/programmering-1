package Assignment_2;

public class Oppgave_2 {
    private double priceA;
    private double weightA;
    private double priceB;
    private double weightB;

    public Oppgave_2(double priceA, double weightA, double priceB, double weightB) {
        this.priceA = priceA;
        this.weightA = weightA;
        this.priceB = priceB;
        this.weightB = weightB;

    }

    public double getPricePerGramA() {
        return (priceA / weightA);
    }

    public double getPricePerGramB() {
        return (priceB / weightB);
    }

    public String compareCosts() {
        if (getPricePerGramA() == getPricePerGramB()) {
            return "Brands A and B have the same price per gram";
        } else if (getPricePerGramA() < getPricePerGramB()) {
            return "Brand A is cheaper";
        } else {
            return "Brand B is cheaper";
        }
    }

    public static void main(String[] args) {
        Oppgave_2 test = new Oppgave_2(35.90, 450.0, 39.50, 500.0);
        System.out.println(test.compareCosts());
        if (test.compareCosts() == "Brand B is cheaper") {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
    }

}