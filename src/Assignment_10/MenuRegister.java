package Assignment_10;

import java.util.ArrayList;

public class MenuRegister { //USE COMPOSITION ARROWS IN CLASS DIAGRAM
    public ArrayList<Menu> menus;
    public ArrayList<Dish> dishes;

    public MenuRegister() {
        menus = new ArrayList<>();
        dishes = new ArrayList<>();
    }

    public void registerNewDish(String dishName, String dishType, int dishPrice) {
        Dish dish = new Dish(dishName, dishType, dishPrice);
        if (!dishes.contains(dish)) {
            dishes.add(dish);
        }
    }

    public void registerMenuFromRegisteredDishes(Dish appetizer, Dish main, Dish dessert) throws IllegalArgumentException{
        if (!dishes.contains(appetizer) || !dishes.contains(main) || !dishes.contains(dessert)) {
            throw new IllegalArgumentException("Dish not registered");
        }
        Menu menu = new Menu(appetizer, main, dessert);
        menus.add(menu);
    }

    public void registerMenuWithoutRegisteredDishes(String appetizer, int appetizerPrice, String main, int mainPrice, String dessert, int dessertPrice) {
        registerNewDish(appetizer, "Appetizer", appetizerPrice);
        registerNewDish(main, "Main", mainPrice);
        registerNewDish(dessert, "Dessert", dessertPrice);

        Dish appDish = new Dish(appetizer, "Appetizer", appetizerPrice);
        Dish mainDish = new Dish(main, "Main", mainPrice);
        Dish dessertDish = new Dish(dessert, "Dessert", dessertPrice);

        Menu menu = new Menu(appDish, mainDish, dessertDish);
        menus.add(menu);
    }

    public Dish getDishByName(String dishName) {
        for (Dish dish : dishes) {
            if (dish.getDishName().equals(dishName)) {
                return dish;
            }
        } return null;
    }

    public ArrayList<Dish> getDishByType(String dishType) {
        ArrayList<Dish> dishesOfType = new ArrayList<>();
        for (Dish dish : dishes) {
            if (dish.getDishType().equals(dishType)) {
                dishesOfType.add(dish);
            }
        } if (dishesOfType.isEmpty()) {
            return null;
        } return dishesOfType;
    }

    public ArrayList<Menu> getMenusByPrice(int startPrice, int endPrice) {
        ArrayList<Menu> menusByPrice = new ArrayList<>();
        for (Menu menu : menus) {
            if (menu.getMenuPrice() >= startPrice && menu.getMenuPrice() <= endPrice) {
                menusByPrice.add(menu);
            }
        }if (menusByPrice.isEmpty()) {
            return null;
        } return menusByPrice;
    }
}
