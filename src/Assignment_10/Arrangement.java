package Assignment_10;

import java.time.LocalDateTime;

public class Arrangement {
    private final int number;
    private final String name;
    private final String location;
    private final String arranger;
    private final String type;
    private final LocalDateTime dateTime;
    private final int dateTimeInteger;

    public Arrangement(int number, String name, String location, String arranger, String type, int year, int month, int day, int hour, int minute) {
        this.number = number;
        this.name = name;
        this.location = location;
        this.arranger = arranger;
        this.type = type;
        this.dateTime = LocalDateTime.of(year, month, day, hour, minute);

        if(day < 10) {
            day = Integer.parseInt("0" + day);
        }
        if(month < 10) {
            month = Integer.parseInt("0" + month);
        }
        this.dateTimeInteger = Integer.parseInt("" + year + month + day + hour + minute);

    }
    public int getNumber() {
        return this.number;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public String getArranger() {
        return this.arranger;
    }

    public String getType() {
        return this.type;
    }

    public LocalDateTime getDateTime() {
        return this.dateTime;
    }

    public int getDateTimeInteger() {
        return this.dateTimeInteger;
    }

    public String toString() {
        return "Arrangement number: " + getNumber() + "\n" +
                "Name: " + getName() + "\n" +
                "Location: " + getLocation() + "\n" +
                "Arranger: " + getArranger() + "\n" +
                "Type: " + getType() + "\n" +
                "Date and time: " + getDateTime() + "\n";
    }
}
