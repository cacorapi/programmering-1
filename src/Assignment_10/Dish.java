package Assignment_10;

import java.util.Objects;

public class Dish {
    private final String name;
    private final String type;
    private final int price;

    public Dish(String dishName, String dishType, int dishPrice) throws IllegalArgumentException{
        if (dishPrice < 0) throw new IllegalArgumentException("Price cannot be negative");
        if (!Objects.equals(dishType, "Appetizer") && !Objects.equals(dishType, "Main") && !Objects.equals(dishType, "Dessert")) throw new IllegalArgumentException("Dish type must be Appetizer, Main or Dessert");

        this.name = dishName;
        this.type = dishType;
        this.price = dishPrice;
    }
    public String getDishName() {
        return this.name;
    }
    public String getDishType() {
        return this.type;
    }
    public int getDishPrice() {
        return this.price;
    }

    public String toString() {
        return "Dish name: " + getDishName() + "\n" +
                "Dish type: " + getDishType() + "\n" +
                "Dish price: " + getDishPrice() + "\n";
    }
}
