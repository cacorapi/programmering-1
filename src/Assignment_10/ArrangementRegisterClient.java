package Assignment_10;

import java.util.Scanner;
public class ArrangementRegisterClient {
    public static String registrationMenu() {
        return "Please select one of the options below:\n" +
                "1. Add new arrangement\n" +
                "2. List all arrangements in a given location\n" +
                "3. List all arrangements on a given day\n" +
                "4. List all arrangements within a given time interval\n" +
                "5. List all arrangements\n" +
                "6. Quit Menu";
    }
    public static void main(String[] args) throws IllegalArgumentException {
        int choice;
        Scanner scanner = new Scanner(System.in);
        ArrangementRegister register = new ArrangementRegister();
        System.out.println("Welcome to the registry for local arrangements");

        do {
            System.out.println(registrationMenu());
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("To add a new arrangement, please enter the following details:");
                    System.out.println("Name of arrangement:");
                    String name1 = scanner.nextLine();
                    System.out.println("Location:");
                    String location1 = scanner.nextLine();
                    System.out.println("Arranger:");
                    String arranger1 = scanner.nextLine();
                    System.out.println("Type:");
                    String type1 = scanner.nextLine();
                    System.out.println("Year:");
                    int year1 = scanner.nextInt();
                    System.out.println("Month:");
                    int month1 = scanner.nextInt();
                    System.out.println("Day:");
                    int day1 = scanner.nextInt();
                    scanner.nextLine();

                    System.out.println("Start hour:");
                    int hour1 = scanner.nextInt();
                    System.out.println("Start minute:");
                    int minute1 = scanner.nextInt();

                    register.registerNewArrangement(name1, location1, arranger1, type1, year1, month1, day1, hour1, minute1);
                    System.out.println("Arrangement added!");
                    break;
                case 2:
                    System.out.println("Please enter the location you want to list arrangements for:");
                    String location2 = scanner.nextLine();
                    System.out.println(register.getArrangementsAtLocation(location2));
                    break;
                case 3:
                    System.out.println("Please enter the date you want to list arrangements for:");
                    System.out.println("Year:");
                    int year2 = scanner.nextInt();
                    System.out.println("Month:");
                    int month2 = scanner.nextInt();
                    System.out.println("Day:");
                    int day2 = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println(register.getArrangementsOnDay(year2, month2, day2));
                    break;
                case 4:
                    System.out.println("Please enter the time interval you want to list arrangements for:");
                    System.out.println("Start year:");
                    int year3 = scanner.nextInt();
                    System.out.println("Start month:");
                    int month3 = scanner.nextInt();
                    System.out.println("Start day:");
                    int day3 = scanner.nextInt();
                    scanner.nextLine();

                    System.out.println("Start hour:");
                    int hour3 = scanner.nextInt();
                    System.out.println("Start minute:");
                    int minute3 = scanner.nextInt();
                    scanner.nextLine();

                    System.out.println("End year:");
                    int year4 = scanner.nextInt();
                    System.out.println("End month:");
                    int month4 = scanner.nextInt();
                    System.out.println("End day:");
                    int day4 = scanner.nextInt();
                    scanner.nextLine();

                    System.out.println("End hour:");
                    int hour4 = scanner.nextInt();
                    System.out.println("End minute:");
                    int minute4 = scanner.nextInt();

                    System.out.println(register.getArrangementsInTimeInterval(year3, month3, day3, hour3, minute3, year4, month4, day4, hour4, minute4));
                    break;
                case 5:
                    System.out.println(register.getSortedArrangements());
                    break;
                default:
                    if (choice != 6) {
                        System.out.println("Invalid input, please try again");
                        scanner.nextLine();
                    }
                    break;
            }

        } while (choice != 6);
        System.out.println("Goodbye!");
        scanner.close();
        System.exit(0);
    }
}
