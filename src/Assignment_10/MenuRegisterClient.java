package Assignment_10;

import java.util.Scanner;
public class MenuRegisterClient {

    public static String RegistryOptions() {
        return "1. Register new dish\n" +
                "2. Make menu from registered dishes\n" +
                "3. Register entire menu\n" +
                "4. Find a dish's information by name\n" +
                "5. Find all dishes of a type\n" +
                "6. Get menus within a set price interval\n" +
                "7. Exit\n" +
                "Enter your choice: ";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MenuRegister register = new MenuRegister();
        int choice;

        System.out.println("Welcome to the menu registry!");

        do {
            System.out.println(RegistryOptions());
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("To register a new dish, please enter the following details:");
                    System.out.println("Name of dish:");
                    String name1 = scanner.nextLine();
                    System.out.println("Type of dish (Choose 1 for appetizer, 2 for main, 3 for dessert):");
                    int type1Int = scanner.nextInt();
                    String type1;
                    if (type1Int == 1) {
                        type1 = "Appetizer";
                    } else if (type1Int == 2) {
                        type1 = "Main";
                    } else if (type1Int == 3) {
                        type1 = "Dessert";
                    } else {
                        System.out.println("Invalid choice, please try again");
                        break;
                    }
                    System.out.println("Price of dish (NOK):");
                    int price1 = scanner.nextInt();
                    scanner.nextLine();
                    register.registerNewDish(name1, type1, price1);
                    break;
                case 2:
                    System.out.println("To make a menu from the pre-registered dishes, please enter the following details:");
                    System.out.println("Appetizer name: ");
                    String appetizer2 = scanner.nextLine();
                    System.out.println("Main: ");
                    String main2 = scanner.nextLine();
                    System.out.println("Dessert: ");
                    String dessert2 = scanner.nextLine();
                    register.registerMenuFromRegisteredDishes(register.getDishByName(appetizer2), register.getDishByName(main2), register.getDishByName(dessert2));
                    break;
                case 3:
                    System.out.println("To make a menu without pre-registered dishes, please enter the following details:");
                    System.out.println("Appetizer name: ");
                    String appetizer3 = scanner.nextLine();
                    System.out.println("Appetizer price  (NOK): ");
                    int appetizerPrice3 = scanner.nextInt();

                    System.out.println("Main name: ");
                    String main3 = scanner.nextLine();
                    scanner.nextLine();
                    System.out.println("Main price (NOK): ");
                    int mainPrice3 = scanner.nextInt();

                    System.out.println("Dessert name: ");
                    String dessert3 = scanner.nextLine();
                    scanner.nextLine();
                    System.out.println("Dessert price (NOK): ");
                    int dessertPrice3 = scanner.nextInt();
                    scanner.nextLine();

                    register.registerMenuWithoutRegisteredDishes(appetizer3, appetizerPrice3, main3, mainPrice3, dessert3, dessertPrice3);
                    break;
                case 4:
                    System.out.println("To find a dish's information by name, please enter the name of the dish:");
                    String name4 = scanner.nextLine();
                    System.out.println(register.getDishByName(name4));
                    break;
                case 5:
                    System.out.println("To find all dishes of a type, please enter the type of the dish (Choose 1 for appetizer, 2 for main, 3 for dessert):");
                    int type5Int = scanner.nextInt();
                    String type5;
                    if (type5Int == 1) {
                        type5 = "Appetizer";
                    } else if (type5Int == 2) {
                        type5 = "Main";
                    } else if (type5Int == 3) {
                        type5 = "Dessert";
                    } else {
                        System.out.println("Invalid choice, please try again");
                        break;
                    }
                    System.out.println(register.getDishByType(type5));
                    break;
                case 6:
                    System.out.println("To get menus within a set price interval, please enter the following details:");
                    System.out.println("Start price  (NOK): ");
                    int startPrice6 = scanner.nextInt();
                    System.out.println("End price  (NOK): ");
                    int endPrice6 = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println(register.getMenusByPrice(startPrice6, endPrice6));
                    break;
                default:
                    if (choice != 7) {
                        System.out.println("Invalid choice, please try again");
                    }
            }
        } while (choice != 7);
        System.out.println("Goodbye!");
        scanner.close();
        System.exit(0);
    }
}
