package Assignment_10;

import java.util.ArrayList;

public class Menu {
    private final ArrayList<Dish> menu;


    public Menu(Dish appetizer, Dish main, Dish dessert) {
        menu = new ArrayList<>();
        menu.add(appetizer);
        menu.add(main);
        menu.add(dessert);

    }

    public String getAppetizer() {
        return this.menu.get(0).getDishName();
    }

    public String getMain() {
        return this.menu.get(1).getDishName();
    }

    public String getDessert() {
        return this.menu.get(2).getDishName();
    }

    public int getMenuPrice() {
        int menuPrice = 0;
        for (Dish dish : this.menu) {
            menuPrice += dish.getDishPrice();
        }
        return menuPrice;
    }

    public String toString() {
        return "Appetizer: " + getAppetizer() + "\n" +
                "Main: " + getMain() + "\n" +
                "Dessert: " + getDessert() + "\n" +
                "Menu price: " + getMenuPrice() + "\n";
    }
}
