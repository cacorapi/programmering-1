package Assignment_10;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

public class ArrangementRegister {
    private final ArrayList<Arrangement> arrangements;
    
    public ArrangementRegister() {
        this.arrangements = new ArrayList<>();
    }
    public void registerNewArrangement(String name, String location, String arranger, String type, int year, int month, int day, int hour, int minute) {
        int number = arrangements.size() + 1;
        Arrangement newArrangement = new Arrangement(number, name, location, arranger, type, year, month, day, hour, minute);
        arrangements.add(newArrangement);
    }

    public ArrayList<Arrangement> sortArrangementsByLocation(ArrayList<Arrangement> arrangementsToSort) {
        ArrayList<Arrangement> arrangementsByLocation = new ArrayList<>(arrangementsToSort);

        Collections.sort(arrangementsByLocation, Comparator.comparing(Arrangement::getLocation));
        return arrangementsByLocation;
    }

    public ArrayList<Arrangement> sortArrangementsByType(ArrayList<Arrangement> arrangementsToSort) {
        ArrayList<Arrangement> arrangementsByType = new ArrayList<>(arrangementsToSort);

        Collections.sort(arrangementsByType, Comparator.comparing(Arrangement::getType));
        return arrangementsByType;
    }

    public ArrayList<Arrangement> sortArrangementsByTime(ArrayList<Arrangement> arrangementsToSort) {
        ArrayList<Arrangement> arrangementsByTime = new ArrayList<>(arrangementsToSort);

        Collections.sort(arrangementsByTime, Comparator.comparing(Arrangement::getDateTimeInteger));
        return arrangementsByTime;
    }

    public ArrayList<Arrangement> getArrangementsAtLocation(String location) {
        ArrayList<Arrangement> arrangementsAtLocation = arrangements.stream()
                .filter(arrangement -> arrangement.getLocation().contains(location))
                .collect(Collectors.toCollection(ArrayList::new));
        return arrangementsAtLocation;
    }

    public ArrayList<Arrangement> getArrangementsOnDay(int year, int month, int day) {
        ArrayList<Arrangement> arrangementsOnDay = arrangements.stream()
                .filter(arrangement -> arrangement.getDateTime().toLocalDate().isEqual(LocalDate.of(year, month, day)))
                .collect(Collectors.toCollection(ArrayList::new));
        return arrangementsOnDay;
    }

    public ArrayList<Arrangement> getArrangementsInTimeInterval(int year1, int month1, int day1, int hour1, int minute1, int year2, int month2, int day2, int hour2, int minute2) {
        LocalDateTime startDateTime = LocalDateTime.of(year1, month1, day1, hour1, minute1);
        LocalDateTime endDateTime = LocalDateTime.of(year2, month2, day2, hour2, minute2);

        ArrayList<Arrangement> arrangementsInInterval = arrangements.stream()
                .filter(arrangement -> (arrangement.getDateTime().isEqual(startDateTime) || arrangement.getDateTime().isAfter(startDateTime))
                        && (arrangement.getDateTime().isEqual(endDateTime) || arrangement.getDateTime().isBefore(endDateTime)))
                .collect(Collectors.toCollection(ArrayList::new));

        return arrangementsInInterval;
    }

    public ArrayList<Arrangement> getSortedArrangements() {
        ArrayList<Arrangement> sortedByLocation = sortArrangementsByLocation(arrangements);
        ArrayList<Arrangement> sortedByType = sortArrangementsByType(sortedByLocation);
        return sortArrangementsByTime(sortedByType);
    }
}
