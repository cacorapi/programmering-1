package Assignment_3;

public class Oppgave_1 {

    private static int start;
    private static int stop;

    public Oppgave_1(int start, int stop) throws IllegalArgumentException {
        if (start > stop) {
            throw new IllegalArgumentException("Start value cannot be greater than stop value");
        }
    }

    public void setProduct() {
        int product;

        for (int i = start; i <= stop; i++) {
            System.out.println("");
            System.out.println("Multiplication table for " + String.valueOf(i) + ":");
            System.out.println("");
            for (int k = 1; k <= 10; k++){
                product = i * k;
                System.out.println(String.valueOf(i) + "x" + String.valueOf(k) + "=" + String.valueOf(product));
            }
        }
    }


    public static void main(String[] args) {
        start = Integer.parseInt(System.console().readLine("Enter start value: "));
        stop = Integer.parseInt(System.console().readLine("Enter stop value: "));
        Oppgave_1 test = new Oppgave_1(start, stop);
        test.setProduct();

    }

}