package Assignment_3;

public class Oppgave_2 {
    private static int number;

    public Oppgave_2()throws IllegalArgumentException {
        if (number <= 0) {
            throw new IllegalArgumentException("Number cannot be negative");
        }
    }

    public boolean isPrime() {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        number = Integer.parseInt(System.console().readLine("Enter number to check if it is prime: "));
        try{
            Oppgave_2 test = new Oppgave_2();
            if (test.isPrime()) {
                System.out.println("Number is prime");
            } else {
                System.out.println("Number is not prime");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}

