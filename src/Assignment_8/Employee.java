package Assignment_8;

public class Employee {
    java.util.GregorianCalendar calendar= new java.util.GregorianCalendar();
    private final int year = calendar.get(java.util.Calendar.YEAR);
    private Person personalia;
    private int employeeNumber;
    private int employmentYear;
    private double monthlySalary;
    private double taxDeduction;

    public Employee(Person personalia, int employeeNumber,int employmentYear, double monthlySalary, double taxDeduction) {
        this.personalia = personalia;
        this.employeeNumber = employeeNumber;
        this.employmentYear = employmentYear;
        this.monthlySalary = monthlySalary;
        this.taxDeduction = taxDeduction;
    }

    public void setPersonalia(Person personalia) {
        this.personalia = personalia;
    }
    public Person getPersonalia() {
        return this.personalia;
    }

    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }
    public int getEmployeeNumber() {
        return this.employeeNumber;
    }

    public void setEmploymentYear(int employmentYear) {
        this.employmentYear = employmentYear;
    }
    public int getEmploymentYear() {
        return this.employmentYear;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }
    public double getMonthlySalary() {
        return this.monthlySalary;
    }

    public void setTaxDeduction(double taxDeduction) {
        this.taxDeduction = taxDeduction;
    }
    public double getTaxDeduction() {
        return this.taxDeduction;
    }

    public double getMonthlyTax(){
        return getMonthlySalary() * taxDeduction;
    }

    public double getGrossSalary(){
        return getMonthlySalary() * 12;
    }

    public double getAnnualTax(){
        return getMonthlyTax() * 10.5;
    }

    public int getAge(){
        return year - personalia.getBirthYear();
    }

    public String getEmployeeName() {
        return personalia.getSurname() + ", " + personalia.getFirstName();
    }

    public int getYearsEmployed(){
        return year - getEmploymentYear();
    }

    public boolean employedLongerThan(int years){
        return getYearsEmployed() > years;
    }

    @Override
    public String toString() {
        return "Employee{" +
                " personalia: " + personalia +
                ", employeeNumber = " + employeeNumber +
                ", employmentYear = " + employmentYear +
                ", monthlySalary = " + monthlySalary +
                ", taxDeduction = " + taxDeduction +
                '}';
    }
}
