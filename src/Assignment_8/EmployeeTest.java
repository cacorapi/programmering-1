package Assignment_8;

public class EmployeeTest {
    public static void main(String[] args) {
        Employee test  = new Employee(new Person("Charlotte", "Corapi", 2002), 1, 2020, 10000, 0.2);
        if (test.getPersonalia().getFirstName().equals("Charlotte") && test.getPersonalia().getSurname().equals("Corapi") && test.getPersonalia().getBirthYear() == 2002 && test.getEmployeeNumber() == 1 && test.getEmploymentYear() == 2020 && test.getMonthlySalary() == 10000 && test.getTaxDeduction() == 0.2) {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
        System.out.println(test.toString());

    }
}
