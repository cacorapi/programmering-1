package Assignment_8;

public class PersonTest {
    public static void main(String[] args) {
        Person test = new Person("Charlotte", "Corapi", 2002);
        if (test.getFirstName().equals("Charlotte") && test.getSurname().equals("Corapi") && test.getBirthYear() == 2002) {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
        System.out.println(test.toString());
    }
}
