package Assignment_8;

public class Person {
    private final String firstName;
    private final String surname;
    private final int birthYear;

    public Person(String firstName, String surname, int birthYear) {
        this.firstName = firstName;
        this.surname = surname;
        this.birthYear = birthYear;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getSurname() {
        return this.surname;
    }

    public int getBirthYear() {
        return this.birthYear;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName = " + firstName +
                ", surname = " + surname +
                ", birthYear = " + birthYear +'}';
    }
}
