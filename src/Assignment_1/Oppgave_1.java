package Assignment_1;

public class Oppgave_1 {

    private float centimeters;

    public Oppgave_1(float inches) {
        if (inches < 0) {
            throw new IllegalArgumentException("Inches can't be negative");
        }
        this.centimeters = inches * 2.54f;
    }

    public float getCentimeters() {
        return centimeters;
    }

    public static void main(String[] args) {
        Oppgave_1 test = new Oppgave_1(10);
        System.out.println(test.getCentimeters());
        if (test.getCentimeters() == 25.4f) {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
    }


}
