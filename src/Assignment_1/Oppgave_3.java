package Assignment_1;

public class Oppgave_3 {

    private int hours;
    private int minutes;
    private int seconds;

    public Oppgave_3(int totalSeconds){
        hours = totalSeconds / 3600;
        minutes = (totalSeconds % 3600) / 60;
        seconds = totalSeconds - ((minutes * 60) + (hours * 3600));
    }

    public int getHours(){
        return hours;
    }
    public int getMinutes(){
        return minutes;
    }

    public int getSeconds(){
        return seconds;
    }

    public static void main(String[] args) {
        Oppgave_3 test = new Oppgave_3(3661);
        System.out.println(test.getHours());
        System.out.println(test.getMinutes());
        System.out.println(test.getSeconds());
        if (test.getHours() == 1 && test.getMinutes() == 1 && test.getSeconds() == 1) {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
    }
}