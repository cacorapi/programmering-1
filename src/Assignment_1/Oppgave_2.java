package Assignment_1;

public class Oppgave_2 {

    private int totalSeconds;

    public Oppgave_2(int hours, int minutes, int seconds) {
        totalSeconds = hours * 3600 + minutes * 60 + seconds;
    }

    public int getTotalSeconds(){
        return totalSeconds;
    }

    public static void main(String[] args) {
        Oppgave_2 test = new Oppgave_2(1, 1, 1);
        System.out.println(test.getTotalSeconds());
        if (test.getTotalSeconds() == 3661) {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
    }
}