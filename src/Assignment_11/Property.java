package Assignment_11;

public class Property {
    private int municipalityNumber;
    private final String municipalityName;
    private int lotNumber;
    private int sectionNumber;
    private final String name;
    private double area;
    private final String owner;

    /** Constructor for Property class
     * @param municipalityNumber -> municipality number of property
     * @param municipalityName -> municipality name of property
     * @param lotNumber -> lot number of property
     * @param sectionNumber -> section number of property
     * @param name -> name of property
     * @param area -> area of property
     * @param owner -> owner of property
     */
    public Property(int municipalityNumber, String municipalityName, int lotNumber, int sectionNumber, String name, double area, String owner) {
        setMunicipalityNumber(municipalityNumber);
        this.municipalityName = municipalityName;
        setLotNumber(lotNumber);
        setSectionNumber(sectionNumber);
        this.name = name;
        setArea(area);
        this.owner = owner;

    }
    /** Mutator and accessor methods for municipality number
     * @param municipalityNumber -> municipality number of property
     * @throws IllegalArgumentException -> if municipality number is below 101 or above 5054
     * return -> municipality number of property
     */
    public void setMunicipalityNumber(int municipalityNumber) throws IllegalArgumentException {
        if (municipalityNumber <= 101 || municipalityNumber >= 5054) {
            throw new IllegalArgumentException("Municipality number must be between 101 and 5054");
        }
        this.municipalityNumber = municipalityNumber;
    }
    public int getMunicipalityNumber() {
        return municipalityNumber;
    }
    /** Accessor method for municipality name
     * @return -> municipality name of property
     */
    public String getMunicipalityName() {
        return municipalityName;
    }
    /** Mutator and accessor methods for lot number
     * @param lotNumber -> lot number of property
     * @throws IllegalArgumentException -> if lot number is negative
     * return -> lot number of property
     */
    public void setLotNumber(int lotNumber) throws IllegalArgumentException {
        if (lotNumber < 0) {
            throw new IllegalArgumentException("Lot number cannot be negative");
        }
        this.lotNumber = lotNumber;
    }
    public int getLotNumber() {
        return lotNumber;
    }

    /** Mutator and accessor methods for section number
     * @param sectionNumber -> section number of property
     * @throws IllegalArgumentException -> if section number is negative
     * return -> section number of property
     */
    public void setSectionNumber(int sectionNumber) {
        if (sectionNumber < 0) {
            throw new IllegalArgumentException("Section number cannot be negative");
        }
        this.sectionNumber = sectionNumber;
    }
    public int getSectionNumber() {
        return sectionNumber;
    }

    /** Accessor method for property name
     * return -> name of property
     */
    public String getName() {
        return name;
    }

    /** Mutator and accessor methods for area
     * @param area -> area of property
     * @throws IllegalArgumentException -> if area is negative
     * return -> area of property
     */
    public void setArea(double area) throws IllegalArgumentException {
        if (area < 0.00) {
            throw new IllegalArgumentException("Area cannot be negative");
        }
        this.area = area;
    }
    public double getArea() {
        return area;
    }

    /** Accessor method for owner
     * return -> owner of property
     */
    public String getOwner() {
        return owner;
    }

    /** Method to return property ID
     * @return -> property ID
     */
    public String getPropertyID() {
        return Integer.toString(getMunicipalityNumber()) + "-" + Integer.toString(getLotNumber()) + "/" + Integer.toString(getSectionNumber());
    }

    /** Method to return property information
     * @return -> property information
     */

    public String toString() {
        return "Municipality number: " + getMunicipalityNumber() +
                "\nMunicipality name: " + getMunicipalityName() +
                "\nLot number: " + getLotNumber() +
                "\nSection number: " + getSectionNumber() +
                "\nName: " + getName() +
                "\nArea: " + getArea() +
                "\nOwner: " + getOwner();
    }
}
