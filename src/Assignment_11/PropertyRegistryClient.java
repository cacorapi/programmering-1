package Assignment_11;

import java.util.*;

public class PropertyRegistryClient {
    /** Method for presenting the menu to the user and getting the user's choice
     * Presents the menu for the user, and awaits input from the user
     * @return the menu choice by the user as a positive number starting from 1.
     * If the user enters text or a number outside the range 1-8, an error message is printed and the menu is shown again
     */

    private int showMenu()
    {
        int menuChoice = 0;
        System.out.println("\n***** Property Register Application v0.1 *****\n");
        System.out.println("1. Get a list of all properties");
        System.out.println("2. Get the number of properties registered");
        System.out.println("3. Get the average property area in registry");
        System.out.println("4. Register a property");
        System.out.println("5. Remove a property");
        System.out.println("6. Search for a property using its ID (municipalityNumber-lotNumber/SectionNumber)");
        System.out.println("7. List the properties in a given lot");
        System.out.println("8. Quit");
        System.out.println("\nPlease enter a number between 1 and 8.\n");
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            menuChoice = scanner.nextInt();
        } else {
            System.out.println("You must enter a number, not text");
            showMenu();
        }
        if (menuChoice < 1 || menuChoice > 8) {
            System.out.println("You must enter a number between 1 and 8");
            showMenu();
        }
        return menuChoice;
    }

    /**
     * Starts the application. This is the main loop of the application,
     * presenting the menu, retrieving the selected menu choice from the user,
     * and executing the selected functionality.
     */
    public void start() {
        boolean finished = false;

        PropertyRegistry registry = new PropertyRegistry();

        while (!finished) {
            int menuChoice = this.showMenu();
            switch (menuChoice)
            {
                case 1:
                    System.out.println("List of properties in registry:");
                    System.out.println(registry.getProperties());
                    break;
                case 2:
                    System.out.println("There are " + registry.getNumberOfProperties() + " properties registered");
                    break;
                case 3:
                    System.out.println("The average area of all properties in the registry is " + registry.getAverageArea() + " square meters");
                    break;
                case 4:
                    System.out.println("To register a new property enter the property's details in the following format: ");
                    System.out.println("municipalityNumber,municipalityName,lotNumber,sectionNumber,name,area,owner");
                    System.out.println("Example: 1445,Gloppen,77,131,Syningom,663.1,Nicolay Madsen");
                    System.out.println("Remember to only use positive numbers, and that the municipality number must be between 101 and 5054. If there is no name just leave that spot blank.");
                    System.out.println("Example: 1445,Gloppen,77,631,,1017.6,Jens Olsen");
                    System.out.println();
                    System.out.println("Enter the property's details:");
                    Scanner scanner = new Scanner(System.in);
                    String propertyDetails = scanner.nextLine();

                    String[] propertyDetailsArray = propertyDetails.split(",");
                    int municipalityNumber = Integer.parseInt(propertyDetailsArray[0]);
                    String municipalityName = propertyDetailsArray[1];
                    int lotNumber = Integer.parseInt(propertyDetailsArray[2]);
                    int sectionNumber = Integer.parseInt(propertyDetailsArray[3]);
                    String name = propertyDetailsArray[4];
                    double area = Double.parseDouble(propertyDetailsArray[5]);
                    String owner = propertyDetailsArray[6];
                    registry.registerProperty(municipalityNumber, municipalityName, lotNumber, sectionNumber, name, area, owner);
                    System.out.println("Property registered!");
                    break;
                case 5:
                    System.out.println("Enter the ID of the property you want to remove:");
                    Scanner scanner2 = new Scanner(System.in);
                    String propertyID = scanner2.nextLine();
                    registry.removeProperty(propertyID);
                    System.out.println("Property removed!");
                    break;
                case 6:
                    System.out.println("Enter the ID of the property you want to find:");
                    Scanner scanner3 = new Scanner(System.in);
                    String propertyID2 = scanner3.nextLine();
                    System.out.println(registry.getProperty(propertyID2));
                    break;
                case 7:
                    System.out.println("Enter the lot number of the properties you want to find:");
                    Scanner scanner4 = new Scanner(System.in);
                    int lotNumber2 = scanner4.nextInt();
                    System.out.println(registry.getPropertiesInLot(lotNumber2));
                    break;
                case 8:
                    System.out.println("Goodbye!\n");
                    finished = true;
                    break;
                default:
                    System.out.println("Unrecognized menu selected..");
                    break;
            }
        }
    }

    public static void main(String[] args) {
        PropertyRegistryClient client = new PropertyRegistryClient();
        client.start();
    }
}
