package Assignment_11;

import java.util.*;
import java.util.stream.Collectors;
import java.text.DecimalFormat;

public class PropertyRegistry {
    private final ArrayList<Property> properties;

    /** Constructor for PropertyRegistry class
     */
    public PropertyRegistry() {
        this.properties = new ArrayList<>();
    }

    /** Accessor method for getting a list of the properties in the registry
     * @return -> list of properties in the registry
     */
    public ArrayList<Property> getProperties() {
        return properties;
    }

    /** Accessor method for getting the number of properties in the property registry
     * @return -> number of properties in the property registry
     */
    public int getNumberOfProperties() {
        return properties.size();
    }

    /** Accessor method for getting the average area of all properties in the property registry
     * @return -> average area of all properties in the property registry
     */
    public double getAverageArea() {
        double totalArea = 0;
        for (Property property : properties) {
            totalArea += property.getArea();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return Double.parseDouble(decimalFormat.format(totalArea / properties.size()));
    }

    /** Mutator method for creating a Property object and adding it to the property registry
     * @param municipalityNumber -> municipality number of property
     * @param municipalityName -> municipality name of property
     * @param lotNumber -> lot number of property
     * @param sectionNumber -> section number of property
     * @param name -> name of property
     * @param area -> area of property
     * @param owner -> owner of property
     */
    public void registerProperty(int municipalityNumber, String municipalityName, int lotNumber, int sectionNumber, String name, double area, String owner) {
        properties.add(new Property(municipalityNumber, municipalityName, lotNumber, sectionNumber, name, area, owner));
    }

    /** Mutator method for removing a property from the property registry
     * @param propertyID -> ID of property to be removed
     * @throws IllegalArgumentException -> if property ID is null
     */
    public void removeProperty(String propertyID) throws IllegalArgumentException {
        if (propertyID == null) {
            throw new IllegalArgumentException("Property ID cannot be null");
        }
        for (Property property : properties) {
            if (property.getPropertyID().equals(propertyID)) {
                properties.remove(property);
            }
        }
    }

    /** Accessor method for getting a property from the property registry
     * @param propertyID -> ID of property to be returned
     * @throws IllegalArgumentException -> if property ID is null
     * @throws IllegalArgumentException -> if property is not found
     * @return -> property with the given ID
     */
    public Property getProperty(String propertyID) throws IllegalArgumentException {
            if (propertyID.isBlank()) {
                throw new IllegalArgumentException("Property ID cannot be null");
        }
        for (Property property : properties) {
            if (property.getPropertyID().equals(propertyID)) {
                return property;
            }
        } throw new IllegalArgumentException("Property not found");
    }

    /** Accessor method for getting a list of the properties in a given lot
     * @param lotNumber -> lot number of properties to be returned
     * @return -> list of properties in the given lot
     */
    public ArrayList<Property> getPropertiesInLot(int lotNumber) {
        ArrayList<Property> propertiesInLot = properties.stream()
                .filter(Property -> Property.getLotNumber() == lotNumber)
                .collect(Collectors.toCollection(ArrayList::new));
        return propertiesInLot;
    }

}
