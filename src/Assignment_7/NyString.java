package Assignment_7;

public class NyString {

    private final String sentence;

    public NyString(String sentence) {
        this.sentence = sentence;
    }

    public String forkortning() {
        String[] words = this.sentence.split(" ");
        StringBuilder acronym = new StringBuilder();
        for (String word: words) {
            if (!word.isEmpty()) {
                acronym.append(word.charAt(0));
            }
        }
        return acronym.toString().toLowerCase();
    }

    public String fjerningAvTegn(String letter) {
        return this.sentence.replace(letter, "");
    }
}
