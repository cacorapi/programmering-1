package Assignment_7;

public class TextEditorTest {
    public static void main(String[] args) {
        TextEditor test = new TextEditor("The Hare challenged the Tortoise to a race, confident of victory. The Tortoise" +
                " accepted the challenge. They lined up, the race began: The Hare sprinted ahead, confident of success," +
                " and soon he was out of sight. The Tortoise plodded along, slow but determined. The Hare, overconfident," +
                " took a nap under a tree. The Tortoise kept going. Suddenly, the Hare awoke, too late. The Tortoise won" +
                " the race! The lesson? Slow and steady: Wins the race!");
        System.out.println(test.getText());
        System.out.println(test.getUpperCaseText());
        System.out.println(test.getNumberOfWords());
        System.out.println(test.getAverageWordLength());
        System.out.println(test.getAverageClauseLength());
        System.out.println(test.switchWord("Hare", "Rabbit"));

    }
}
