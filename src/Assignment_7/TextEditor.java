package Assignment_7;

public class TextEditor {
    private final String text;

    public TextEditor(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public String getUpperCaseText() {
        return getText().toUpperCase();
    }

    public String[] getWordList() {
        return getText().split(" ");
    }

    public int getNumberOfWords() {
        return getWordList().length;
    }

    public double getAverageWordLength() {
        double totalLetters = 0.0;
        if (getWordList().length == 0) {
            return 0;
        }
        for (String word: getWordList()) {
            totalLetters += word.length();
        }
        return totalLetters/getWordList().length;
    }
    public double getAverageClauseLength() {
        double totalWords = 0.0;
        String[] clauses = getText().split("[.:!?]+");
        if (clauses.length == 0) {
            return 0;
        }
        for (String clause: clauses) {
            String[] words = clause.split("[ ,]+");
            totalWords += words.length;
        }
        return totalWords / clauses.length;
    }

    public String switchWord(String original, String replacement) {
        return this.text.replace(original, replacement);
    }
}
