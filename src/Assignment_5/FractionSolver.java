package Assignment_5;

public class FractionSolver {

    private int numerator;
    private int denominator;
    private int new_numerator;
    private int new_denominator;

    public FractionSolver(int numerator, int denominator) throws IllegalArgumentException{
        if (denominator == 0){
            throw new IllegalArgumentException("The denominator cannot be zero");
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public FractionSolver(int numerator) {
        this.numerator = numerator;
        this.denominator = 1;
    }

    public void operation(String operation, int numerator, int denominator) {
        if (operation == "addition") {
            new_numerator = (this.numerator * denominator) + (numerator * this.denominator);
            new_denominator = this.denominator * denominator;
        }
        else if (operation == "subtraction") {
            new_numerator = (this.numerator * denominator) - (numerator * this.denominator);
            new_denominator = this.denominator * denominator;
        }
        else if (operation == "multiplication") {
            new_numerator = this.numerator * numerator;
            new_denominator = this.denominator * denominator;
        }
        else if (operation == "division") {
            new_numerator = this.numerator * denominator;
            new_denominator = this.denominator * numerator;
        }
    }

    public String getSum(int numerator, int denominator) {
        operation("addition", numerator, denominator);
        String answer = String.valueOf(new_numerator) + '/' + String.valueOf(new_denominator);
        return (String.valueOf(this.numerator) + '/' + String.valueOf(this.denominator) + " + " + String.valueOf(numerator) + '/' + String.valueOf(denominator) + " = " + answer);
    }

    public String getDifference(int numerator, int denominator){
        operation("subtraction", numerator, denominator);
        String answer = String.valueOf(new_numerator) + '/' + String.valueOf(new_denominator);
        return (String.valueOf(this.numerator) + '/' + String.valueOf(this.denominator) + " - " + String.valueOf(numerator) + '/' + String.valueOf(denominator) + " = " + answer);
    }

    public String getProduct(int numerator, int denominator) {
        operation("multiplication", numerator, denominator);
        String answer = String.valueOf(new_numerator) + '/' + String.valueOf(new_denominator);
        return (String.valueOf(this.numerator) + '/' + String.valueOf(this.denominator) + " * " + String.valueOf(numerator) + '/' + String.valueOf(denominator) + " = " + answer);
    }

    public String getQuotient(int numerator, int denominator) {
        operation("division", numerator, denominator);
        String answer = String.valueOf(new_numerator) + '/' + String.valueOf(new_denominator);
        return (String.valueOf(this.numerator) + '/' + String.valueOf(this.denominator) + " / " + String.valueOf(numerator) + '/' + String.valueOf(denominator) + " = " + answer);
    }
}