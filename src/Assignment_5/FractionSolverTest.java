package Assignment_5;

public class FractionSolverTest {
    public static void main(String[] args) {
        FractionSolver test = new FractionSolver(5, 9);
        System.out.println();

        System.out.println(test.getSum(3, 4));
        if (test.getSum(3, 4).contains("47/36")) {
            System.out.println("Test on getSum() passed");
        } else{
            System.out.println("Test on getSum() failed");
        } System.out.println();

        System.out.println(test.getDifference(3, 4));
        if (test.getDifference(3, 4).contains("-7/36")) {
            System.out.println("Test on getDifference() passed");
        } else{
            System.out.println("Test on getDifference() failed");
        }System.out.println();

        System.out.println(test.getProduct(3, 4));
        if (test.getProduct(3, 4).contains("15/36")) {
            System.out.println("Test on getProduct() passed");
        } else{
            System.out.println("Test on getProduct() failed");
        }System.out.println();

        System.out.println(test.getQuotient(3, 4));
        if (test.getQuotient(3, 4).contains("20/27")) {
            System.out.println("Test on getQuotient() passed");
        } else{
            System.out.println("Test on getQuotient() failed");
        }
        System.out.println();
    }

}

