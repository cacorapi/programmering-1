package Assignment_5;

public class MinRandomTest {
    public static void main(String[] args) {
        MinRandom test = new MinRandom();
        System.out.println();

        int lowerIntBoundary = 6;
        int upperIntBoundary = 60;
        int integer = test.nesteHeltall(lowerIntBoundary, upperIntBoundary);
        System.out.println("Lower boundary: " + String.valueOf(lowerIntBoundary) + ", Upper boundary: " + String.valueOf(upperIntBoundary) + ", Generated value: " + String.valueOf(integer));
        if (integer >= lowerIntBoundary && integer <= upperIntBoundary){
            System.out.println("Test on nesteHeltall passed");
        } else {
            System.out.println("Test on nesteHeltall failed");
        }

        System.out.println();

        double lowerDecBoundary = 6.0;
        double upperDecBoundary = 60.0;
        double decimal = test.nesteDesimaltall(lowerDecBoundary, upperDecBoundary);
        System.out.println("Lower boundary: " + String.valueOf(lowerDecBoundary) + ", Upper boundary: " + String.valueOf(upperDecBoundary) + ", Generated value: " + String.valueOf(decimal));
        if (decimal >= lowerDecBoundary && decimal <= upperDecBoundary){
            System.out.println("Test on nesteDesimaltall passed");
        } else {
            System.out.println("Test on nesteDesimaltall failed");
        }
        System.out.println();
    }

}
