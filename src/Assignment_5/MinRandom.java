package Assignment_5;

import java.util.Random;

public class MinRandom {

    private Random value = new Random();

    public MinRandom() {}

    public int nesteHeltall(int lower, int upper) {
        return lower + value.nextInt(upper - lower+1);

    }

    public double nesteDesimaltall(double lower, double upper) {
        double temp = value.nextDouble();
        return lower + (upper-lower) * temp;

    }
}
