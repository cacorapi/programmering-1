package Assignment_4;
import java.util.Random;

public class DiceGame {

    private int sumPoeng;
    private String player;
    private Random terning= new java.util.Random();

    public DiceGame(String player) {
        this.sumPoeng = 0;
        this.player = player;
    }

    public int kastTerningen(){
        int terningkast = terning.nextInt(6);
        return terningkast;
    }
    public int getSumPoeng(){
        if (kastTerningen() == 1) {
            return sumPoeng = 0;
        } else if ((sumPoeng + kastTerningen()) > 100){
            return sumPoeng -= kastTerningen();
        } else {
            return sumPoeng += kastTerningen();
        }

    }

    public boolean erFerdig() {
        if (getSumPoeng() == 100){
            System.out.println("Player " + player + " wins!");
            return getSumPoeng() == 100;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        DiceGame playerA = new DiceGame("A");
        DiceGame playerB = new DiceGame("B");

        int i = 0;

        while (!playerA.erFerdig() && !playerB.erFerdig()) {
            System.out.println("Round " + String.valueOf(i));
            System.out.println("Player A: " + playerA.getSumPoeng());
            System.out.println("Player B: " + playerB.getSumPoeng());
            i++;
        }
    }
}
