package Assignment_4;

public class Valuta {

    private static int currency;
    private String currencyName;
    private double amount;
    private int toOrFrom;
    private double value;

    public Valuta(int currency) throws IllegalArgumentException {
        if (currency < 1 || currency > 4) {
            throw new IllegalArgumentException("Please choose between the currencies shown");
        } else if (currency == 4) {
            System.exit(0);
        }
        if (currency == 1) {
            currencyName = "USD";
        } else if (currency == 2) {
            currencyName = "EUR";
        } else if (currency == 3) {
            currencyName = "SEK";
        }
    }

    public String convertToNOK() {
        if (currency ==1) {
            value = amount * 10.66;
        } else if (currency ==2) {
            value = amount * 11.43;
        } else if (currency ==3) {
            value = amount * 0.96;
        }
        return (String.valueOf(amount) + " " + currencyName + " is " +  value + " NOK");
    }

    public String convertFromNOK() {
        System.out.println();
        if (currency ==1) {

            value = amount * 0.094;
        } else if (currency ==2) {
            value = amount * 0.087;
        } else if (currency ==3) {
            value = amount * 1.04;
        }
        return (String.valueOf(amount) + " NOK " + " is " +  value + " " + currencyName);
    }

    public static void presentOptions() {
        System.out.println("");
        System.out.println("1: Dollar (USD)");
        System.out.println("2: Euro (EUR)");
        System.out.println("3: Swedish Kroner (SEK)");
        System.out.println("Enter 4 to quit the program");
    }

    public static void main(String[] args) {
        presentOptions();
        currency = Integer.parseInt(System.console().readLine("Choose the currency you want to convert to/from: "));

        Valuta test = new Valuta(currency);

        test.amount = Integer.parseInt(System.console().readLine("Enter amount: "));
        test.toOrFrom = Integer.parseInt(System.console().readLine("Enter 1 to convert to NOK, or 2 to convert from NOK: "));
        if (test.toOrFrom == 1) {
            System.out.println(test.convertToNOK());
        } else if (test.toOrFrom == 2) {
            System.out.println(test.convertFromNOK());
        }
    }
}