package Assignment_6;

public class MatricesTest {

    public static void main(String[] args) {
        testMatrixInitialization();
        testMatrixOperations();
    }

    public static void testMatrixInitialization() {
        try {
            Matrices matrices = new Matrices(3, 3);

            int[][] firstMatrix = matrices.getFirstMatrix();
            int[][] secondMatrix = matrices.getSecondMatrix();

            // Print the matrices to verify initialization
            System.out.println("First Matrix:");
            printMatrix(firstMatrix);

            System.out.println("Second Matrix:");
            printMatrix(secondMatrix);
        } catch (IllegalArgumentException e) {
            System.out.println("Test failed: " + e.getMessage());
        }
    }

    public static void testMatrixOperations() {
        try {
            Matrices matrices = new Matrices(3, 3);

            int[][] sumMatrix = matrices.getSum();
            int[][] productMatrix = matrices.getProduct();
            int[][] transposeMatrix = matrices.getTranspose();

            // Print the result matrices to verify operations
            System.out.println("Sum Matrix:");
            printMatrix(sumMatrix);

            System.out.println("Product Matrix:");
            printMatrix(productMatrix);

            System.out.println("Transpose Matrix:");
            printMatrix(transposeMatrix);
        } catch (IllegalArgumentException e) {
            System.out.println("Test failed: " + e.getMessage());
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int[] row : matrix) {
            for (int element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}