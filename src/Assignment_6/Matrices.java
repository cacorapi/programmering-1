package Assignment_6;

import java.util.Random;

public class Matrices {

    private Random random = new java.util.Random();
    private int[][] firstMatrix;
    private int[][] secondMatrix;
    private int rows;
    private int columns;

    public Matrices(int rows, int columns) throws IllegalArgumentException{
        if (rows <= 0 || columns <= 0) {
            throw new IllegalArgumentException("The number of rows and columns must be greater than 0");
        }
        this.rows = rows;
        this.columns = columns;

        this.firstMatrix = new int[rows][columns];
        this.secondMatrix = new int[rows][columns];

        for (int i = 0; i < rows ; i++) {
            for (int j = 0; j < columns; j++) {
                this.firstMatrix[i][j] = random.nextInt(10);
                this.secondMatrix[i][j] = random.nextInt(10);
            }
        }

    }

    public int[][] getFirstMatrix() {
        return this.firstMatrix;
    }

    public int[][] getSecondMatrix() {
        return this.secondMatrix;

    }

    public int[][] getSum() throws IllegalArgumentException {

        if (getFirstMatrix().length != getSecondMatrix().length || getFirstMatrix()[0].length != getSecondMatrix()[0].length) {
            throw new IllegalArgumentException("The matrices must have the same number of rows and columns");
        }

        int[][] newMatrix = new int[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                newMatrix[i][j] = getFirstMatrix()[i][j] + getSecondMatrix()[i][j];
            }
        }

        return newMatrix;
    }

    public int[][] getProduct() throws IllegalArgumentException {

        if (getFirstMatrix()[0].length != getSecondMatrix().length) {
            throw new IllegalArgumentException("The number of columns in the first matrix must be equal to the number of rows in the second matrix");
        }

        int[][] newMatrix = new int[getFirstMatrix().length][getSecondMatrix()[0].length];

        for (int i = 0; i < getFirstMatrix().length; i++) {
            for (int j = 0; j < getSecondMatrix()[0].length; j++) {
                int sum = 0;

                for (int k = 0; k < getFirstMatrix()[0].length; k++) {
                    sum += getFirstMatrix()[i][k] * getSecondMatrix()[k][j];
                }

                newMatrix[i][j] = sum;
            }
        }

        return newMatrix;
    }

    public int[][] getTranspose() {
        int[][] newMatrix = new int[getFirstMatrix()[0].length][getFirstMatrix().length];

        for (int i = 0; i < getFirstMatrix().length; i++) {
            for (int j = 0; j < getFirstMatrix()[0].length; j++) {
                newMatrix[j][i] = getFirstMatrix()[i][j];
            }
        }
        return newMatrix;
    }

}

