package Assignment_6;

import java.util.HashMap;
import java.util.Random;

public class NumberCounter {
    private Random random = new java.util.Random();
    private int loops;
    private HashMap<Integer, Integer> count = new HashMap<>();

    public NumberCounter(int loops) throws IllegalArgumentException {
        if (loops <= 0) {
            throw new IllegalArgumentException("The number of loops must be greater than 0");
        }
        this.loops = loops;
    }

    public String numberFrequency() {
        for (int i = 0; i < loops; i++) {
            int number = random.nextInt(10);
            if (count.containsKey(number)) {
                count.put(number, count.get(number) + 1);
            } else {
                count.put(number, 1);
            }
        } return count.toString();
    }
}