package Assignment_6;

public class NumberCounterTest {

    public static void main(String[] args) {
        NumberCounter number_counter_test1 = new NumberCounter(1000);
        System.out.println("Test with 1000 loops:");
        System.out.println(number_counter_test1.numberFrequency());
        System.out.println();

        System.out.println("Test with 5000 loops:");
        NumberCounter number_counter_test2 = new NumberCounter(5000);
        System.out.println(number_counter_test2.numberFrequency());
        System.out.println();

        System.out.println("Test with 10000 loops:");
        NumberCounter number_counter_test3 = new NumberCounter(10000);
        System.out.println(number_counter_test3.numberFrequency());

    }
}